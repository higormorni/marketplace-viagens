import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormCondicaoPage } from './form-condicao';

@NgModule({
  declarations: [
    FormCondicaoPage,
  ],
  imports: [
    IonicPageModule.forChild(FormCondicaoPage),
  ],
})
export class FormCondicaoPageModule {}
