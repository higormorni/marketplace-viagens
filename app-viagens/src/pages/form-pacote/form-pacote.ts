import { PacoteProvider } from './../../providers/pacote/pacote';
import { ExcursaoProvider } from './../../providers/excursao/excursao';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the FormPacotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-pacote',
  templateUrl: 'form-pacote.html',
})
export class FormPacotePage {

  private insert: boolean = false;
  private excursoes: any[];
  form: FormGroup;
  //private excursao: any = {};
  private pacote: any = {
    titulo: '',
    descricao: '',
    preco: 0,
    excursao_id: 0
  };

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private excursaoProvider: ExcursaoProvider,
    private alertController: AlertController,
    private pacoteProvider: PacoteProvider) {
    this.initalize();

    if (this.navParams.get('pacote') != undefined) {
      this.pacote = this.navParams.get('pacote');
      this.fillFields(this.pacote);
      this.insert = false;
    } else
      this.insert = true;

    this.loadExcursoes();

  }

  fillFields(pacote) {
    this.form.controls['id'].setValue(pacote.id);
    this.form.controls['titulo'].setValue(pacote.titulo);
    this.form.controls['descricao'].setValue(pacote.descricao);
    this.form.controls['preco'].setValue(pacote.preco);
    this.form.controls['excursao_id'].setValue(pacote.excursao_id);
    //console.log(excursao.destino_id);
  }

  loadExcursoes() {
    //Fazendo a requisição para a api
    this.excursaoProvider.getAll().subscribe(data => this.excursoes = data['excursoes'],
      error => console.log(error));
  }

  initalize() {
    // Todos os campos criados no formulario deveram ser referenciados aqui, sendo que o nome da propriedade
    // do objeto deverá ser o mesmo que estiver na proriedade formControlName no input html]

    this.form = this.formBuilder.group({
      id: [''],
      titulo: ['', Validators.required],
      descricao: ['', Validators.required],
      preco: ['', Validators.required],
      excursao_id: ['', Validators.required]
    });

  }

  save() {
    //console.log('teste');
    console.log(this.form.valid);
    if (this.insert) {
      this.pacoteProvider.save(this.form.value).subscribe(data => {
        this.success();
      },
        error => console.log(error));
    } else {
      this.pacoteProvider.update(this.form.controls['id'].value, this.form.value).subscribe(data => {
        this.success();
      },
        error => console.log(error));
    }
  }
  updateRegister() {

  }

  newRegister() {

  }

  success() {
    const alert = this.alertController.create({
      title: 'Sucesso',
      message: 'Seu pacote foi ' + (this.insert ? 'cadastrada' : 'atualizada'),
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }
}
