import { ExcursaoProvider } from './../../providers/excursao/excursao';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CategoriaProvider } from "../../providers/categoria/categoria";

/**
 * Generated class for the FormExcursaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-excursao',
  templateUrl: 'form-excursao.html',
})
export class FormExcursaoPage {

  private insert: boolean = false;
  private categorias: any[];
  form: FormGroup;
  //private excursao: any = {};
  private excursao: any = {
    titulo: '',
    chegada: '',
    inicio: '',
    encerramento: '',
    previsao_retorno: '',
    categoria_id: 0,
    destino_id: 0,
    usuario_id: 1
  };

  constructor(
    private formBuilder: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    private categoriaProvider: CategoriaProvider,
    private excursaoProvider: ExcursaoProvider,
    private alertController: AlertController) {
    this.initalize();

    if (this.navParams.get('excursao') != undefined) {
      this.excursao = this.navParams.get('excursao');
      this.fillFields(this.excursao);
      this.insert = false;
    } else
      this.insert = true;

    this.loadCategorias();

  }

  fillFields(excursao) {
    this.form.controls['id'].setValue(excursao.id);
    this.form.controls['titulo'].setValue(excursao.titulo);
    this.form.controls['categoria_id'].setValue(excursao.categoria_id);
    this.form.controls['destino_id'].setValue(excursao.destino_id);
    this.form.controls['inicio'].setValue(excursao.inicio.replace(' ', 'T'));
    this.form.controls['encerramento'].setValue(excursao.encerramento.replace(' ', 'T'));
    this.form.controls['previsao_retorno'].setValue(excursao.previsao_retorno.replace(' ', 'T'));
    this.form.controls['chegada'].setValue(excursao.chegada.replace(' ', 'T'));
    //console.log(excursao.destino_id);
  }

  loadCategorias() {
    //Fazendo a requisição para a api
    this.categoriaProvider.getAll().subscribe(data => this.categorias = data['categorias'],
      error => console.log(error));
  }

  initalize() {
    // Todos os campos criados no formulario deveram ser referenciados aqui, sendo que o nome da propriedade
    // do objeto deverá ser o mesmo que estiver na proriedade formControlName no input html]

    this.form = this.formBuilder.group({
      id: [''],
      titulo: ['', Validators.required],
      chegada: ['', Validators.required],
      inicio: ['', Validators.required],
      encerramento: ['', Validators.required],
      previsao_retorno: ['', Validators.required],
      categoria_id: ['', Validators.required],
      destino_id: ['', Validators.required],
      usuario_id: ['']
    });

  }

  save() {

    console.log(this.form.valid);
    if (this.insert) {
      this.excursaoProvider.save(this.formattedValues(this.form.value)).subscribe(data => {
        this.success();
      },
        error => console.log(error));
    } else {
       this.excursaoProvider.update(this.form.controls['id'].value, this.formattedValues(this.form.value)).subscribe(data => {
        this.success();
      },
        error => console.log(error));
    }
  }

  formattedValues(excursao) {
    excursao.inicio = excursao.inicio.replace('T', ' ').replace('Z', '');
    excursao.chegada = excursao.chegada.replace('T', ' ').replace('Z', '');
    excursao.encerramento = excursao.encerramento.replace('T', ' ').replace('Z', '');
    excursao.previsao_retorno = excursao.previsao_retorno.replace('T', ' ').replace('Z', '');
    excursao.categoria_id = parseInt(excursao.categoria_id);
    excursao.destino_id = parseInt(excursao.destino_id);

    return excursao;
  }

  updateRegister() {

  }

  newRegister() {

  }

  success() {
    const alert = this.alertController.create({
      title: 'Sucesso',
      message: 'Sua excursão foi ' + (this.insert ? 'cadastrada' : 'atualizada'),
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }
}
