import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListDestinoPage } from './list-destino';

@NgModule({
  declarations: [
    ListDestinoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListDestinoPage),
  ],
})
export class ListDestinoPageModule {}
