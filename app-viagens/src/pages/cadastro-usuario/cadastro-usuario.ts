import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the CadastroUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro-usuario',
  templateUrl: 'cadastro-usuario.html',
})
export class CadastroUsuarioPage {
  cadastroUsuario : any = {};

  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public navParams: NavParams) {

    this.cadastroUsuario = this.formBuilder.group({
      nome:['',Validators.required],
      email:['',Validators.required],
      telefone:['',Validators],
      senha:['',Validators.required],
      confirmar:['',Validators.required]
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroUsuarioPage');
  }

}
