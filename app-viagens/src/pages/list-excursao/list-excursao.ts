import { Observable } from 'rxjs/Observable';
import { FormExcursaoPage } from './../form-excursao/form-excursao';
import { ExcursaoProvider } from './../../providers/excursao/excursao';

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, IonicModule } from 'ionic-angular';

/**
 * Generated class for the ListExcursaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-excursao',
  templateUrl: 'list-excursao.html',
})
export class ListExcursaoPage {

  private excursoes: Observable<any[]>;

  constructor(private excursaoProvider: ExcursaoProvider, public navCtrl: NavController, public navParams: NavParams,
    private alertContoller: AlertController) {
    this.getAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListExcursaoPage');
  }

  ionViewWillEnter() {
    this.getAll();
  }
  newExcursao() {
    this.navCtrl.push(FormExcursaoPage);
  }

  getAll() {
    this.excursaoProvider.getAll().subscribe(data => {
      this.excursoes = data['excursoes'];
    },
      error => console.log('error'));
  }

  getById(id) {

  }

  remove(id) {
    this.excursaoProvider.delete(id).subscribe(data => this.ionViewWillEnter());
  }

  edit(id) {

  }

  itemTapped(event, item) {
    console.log(event);
    this.navCtrl.push(FormExcursaoPage, { excursao: item });
  }

  presentConfirm(id) {
    console.log(id);
    const alert = this.alertContoller.create({
      title: 'Excluir excursão',
      message: 'Deseja excluir essa excursão?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            this.remove(id);
          }
        }
      ]
    });
    alert.present();
  }
}
