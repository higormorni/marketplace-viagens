import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListPacotePage } from './list-pacote';

@NgModule({
  declarations: [
    ListPacotePage,
  ],
  imports: [
    IonicPageModule.forChild(ListPacotePage),
  ],
})
export class ListPacotePageModule {}
