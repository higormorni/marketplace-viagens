import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListCondicaoPage } from './list-condicao';

@NgModule({
  declarations: [
    ListCondicaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ListCondicaoPage),
  ],
})
export class ListCondicaoPageModule {}
