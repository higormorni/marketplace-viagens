<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condicao extends Model
{

    protected $table = 'condicoes';
    public $timestamps = true;
    protected $fillable = ['condicao', 'usuario_id'];

    public function usuario()
    {
        return $this->belongsTo('App\Usuario');
    }

    public function excursoes()
    {
        return $this->belongsToMany('App\Excursao', 'condicao_excursao', 'condicao_id', 'excursao_id');
    }

    public function reservas()
    {
        return $this->hasMany('App\Reserva', 'condicao_id');
    }

}