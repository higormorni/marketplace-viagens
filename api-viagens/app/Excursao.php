<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Excursao extends Model
{

    protected $table = 'excursoes';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at', 'chegada', 'inicio', 'encerramento', 'previsao_retorno'];
    protected $fillable = ['titulo', 'chegada', 'inicio', 'encerramento', 'previsao_retorno', 'classificacao', 'categoria_id', 'destino_id', 'usuario_id'];

    public function saidas()
    {
        return $this->hasMany('App\Saida');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }

    public function destino()
    {
        return $this->hasOne('App\Destino');
    }

    public function organizador()
    {
        return $this->belongsTo('App\Usuario');
    }

    public function pacotes()
    {
        return $this->hasMany('App\Pacote');
    }

    public function condicoes_pagamento()
    {
        return $this->belongsToMany('App\Condicao', 'condicao_excursao', 'excursao_id', 'condicao_id');
    }

    /*public function setChegadaAttribute($value) {
        $this->attributes['chegada'] = Carbon::createFromFormat("yyyy-MM-ddTHH:mm:ssZ", $value);
    }

    public function setInicioAttribute($value) {
        $this->attributes['inicio'] = Carbon::createFromFormat("yyyy-MM-ddTHH:mm:ssZ", $value);
    }

    public function setEncerramentoAttribute($value) {
        $this->attributes['encerramento'] = Carbon::createFromFormat("yyyy-MM-ddTHH:mm:ssZ", $value);
    }

    public function setPrevisaoRetornoAttribute($value) {
        $this->attributes['previsao_retorno'] = Carbon::createFromFormat("yyyy-MM-ddTHH:mm:ssZ", $value);
    }*/
}