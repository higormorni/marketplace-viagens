<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Saida extends Model 
{

    protected $table = 'saidas';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['cidade_id', 'excursao_id'];

    public function cidade()
    {
        return $this->belongsTo('App\Cidade');
    }

    public function excursao()
    {
        return $this->belongsTo('App\Excursao');
    }

    public function passageiros()
    {
        return $this->hasManyThrough('App\Usuario', 'App\Reserva');
    }

}