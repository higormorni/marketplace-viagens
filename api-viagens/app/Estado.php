<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estados';
    public $timestamps = false;

    protected $fillable = ['nome', 'sigla'];

    public function cidades()
    {
        return $this->hasMany('App\Cidade');
    }

    public function destinos()
    {
        return $this->hasManyThrough('App\Destino', 'App\Cidade');
    }

    public function saidas()
    {
        return $this->hasManyThrough('App\Saida', 'App\Cidade');
    }

}