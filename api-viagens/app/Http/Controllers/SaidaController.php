<?php

namespace App\Http\Controllers;

use App\Saida;
use Illuminate\Http\Request;

class SaidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $saidas = Saida::all();

        if (!$saidas) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhuma saída foi encontrada',
                'saidas' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'As saídas foram listadas com sucesso',
            'saidas' => $saidas
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO Validação

        $saida = new Saida();
        $saida->fill($request->all());

        if (!$saida->save()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A saída não pôde ser criada',
            ], 500);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A saída foi criada com sucesso',
            'saida' => $saida,
            'show_saida' => url()->route('saidas.show', ['saida' => $saida])
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Saida $saida
     */
    public function show($id)
    {
        $saida = Saida::find($id);

        if (!$saida) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A saída não foi encontrada',
                'saida' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A saída foi encontrada com sucesso',
            'saida' => $saida
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Saida $saida
     */
    public function update(Request $request, $id)
    {
        $saida = Saida::find($id);

        if (!$saida) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A saída não foi encontrada',
                'saida' => null
            ], 404);
        }

        // TODO Validação

        if (!$saida->update($request->all())) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A saída não pôde ser atualizada',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A saída foi atualizada',
            'saida' => $saida,
            'show_saida' => url()->route('saidas.show', ['saida' => $saida])
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Saida $saida
     */
    public function destroy($id)
    {
        $saida = Saida::find($id);

        if (!$saida) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A saída não pôde ser encontrada',
                'saida' => null
            ], 404);
        }

        if (!$saida->delete()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A saída não pôde ser removida',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A saída foi removida',
        ], 200);
    }
}
