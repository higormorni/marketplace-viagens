<?php

namespace App\Http\Controllers;

use App\Condicao;
use Illuminate\Http\Request;

class CondicaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $condicoes = Condicao::all();

        if (!$condicoes) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhuma condição de pagamento foi encontrada',
                'condicoes' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'As condições de pagamento foram listadas com sucesso',
            'condicoes' => $condicoes
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO Validação

        $condicao = new Condicao();
        $condicao->fill($request->all());

        if (!$condicao->save()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A condição de pagamento não pôde ser criada',
            ], 500);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A condição de pagamento foi criada com sucesso',
            'condicao' => $condicao,
            'show_condicao' => url()->route('condicoes.show', ['condicao' => $condicao])
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Condicao $condicao
     */
    public function show($id)
    {
        $condicao = Condicao::find($id);

        if (!$condicao) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A condição de pagamento não foi encontrada',
                'condicao' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A condição de pagamento foi encontrada com sucesso',
            'categoria' => $condicao
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Condicao $condicao
     */
    public function update(Request $request, $id)
    {
        $condicao = Condicao::find($id);

        if (!$condicao) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A condição de pagamento não foi encontrada',
                'condicao' => null
            ], 404);
        }

        // TODO Validação

        if (!$condicao->update($request->all())) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A condição de pagamento não pôde ser atualizada',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A condição de pagamento foi atualizada',
            'condicao' => $condicao,
            'show_condicao' => url()->route('condicoes.show', ['condicao' => $condicao])
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Condicao $condicao
     */
    public function destroy($id)
    {
        $condicao = Condicao::find($id);

        if (!$condicao) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A condição de pagamento não pôde ser encontrada',
                'condicao' => null
            ], 404);
        }

        if (!$condicao->delete()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A condição de pagamento não pôde ser removida',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A condição de pagamento foi removida',
        ], 200);
    }
}
