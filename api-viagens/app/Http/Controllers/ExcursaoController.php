<?php

namespace App\Http\Controllers;

use App\Excursao;
use Illuminate\Http\Request;

class ExcursaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $excursoes = Excursao::all();

        if (!$excursoes) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhuma excursão foi encontrada',
                'excursoes' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'As excursões foram listadas com sucesso',
            'excursoes' => $excursoes
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO Validação

        $excursao = new Excursao();
        $excursao->fill($request->all());

        if (!$excursao->save()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A excursão não pôde ser criada',
            ], 500);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A excursão foi criada com sucesso',
            'excursao' => $excursao,
            'show_excursao' => url()->route('excursoes.show', ['excursao' => $excursao])
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Excursao $excursao
     */
    public function show($id)
    {
        $excursao = Excursao::find($id);

        if (!$excursao) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A excursão não foi encontrada',
                'excursao' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A excursão foi encontrada com sucesso',
            'excursao' => $excursao
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Excursao $excursao
     */
    public function update(Request $request, $id)
    {
        $excursao = Excursao::find($id);

        if (!$excursao) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A excursão não foi encontrada',
                'excursao' => null
            ], 404);
        }

        // TODO Validação

        if (!$excursao->update($request->all())) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A excursão não pôde ser atualizada',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A excursão foi atualizada',
            'excursao' => $excursao,
            'show_excursao' => url()->route('excursoes.show', ['excursao' => $excursao])
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Excursao $excursao
     */
    public function destroy($id)
    {
        $excursao = Excursao::find($id);

        if (!$excursao) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A excursão não pôde ser encontrada',
                'excursao' => null
            ], 404);
        }

        if (!$excursao->delete()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A excursão não pôde ser removida',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A excursão foi removida',
        ], 200);
    }
}
