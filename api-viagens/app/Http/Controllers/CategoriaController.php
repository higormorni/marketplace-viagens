<?php

namespace App\Http\Controllers;

use App\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::all();

        if ($categorias->count() == 0) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhuma categoria encontrada',
                'categorias' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Categorias listadas com sucesso',
            'categorias' => $categorias
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO Validação

        $categoria = new Categoria();
        $categoria->fill($request->all());

        if (!$categoria->save()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A categoria não pôde ser criada',
            ], 500);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A categoria foi criada com sucesso',
            'categoria' => $categoria,
            'show_categoria' => url()->route('categorias.show', ['categoria' => $categoria])
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Categoria $categoria
     */
    public function show($id)
    {
        $categoria = Categoria::find($id);

        if (!$categoria) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Categoria não encontrada',
                'categoria' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Categoria encontrada com sucesso',
            'categoria' => $categoria
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Categoria $categoria
     */
    public function update(Request $request, $id)
    {
        $categoria = Categoria::find($id);

        if (!$categoria) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A categoria não foi encontrada',
                'categoria' => null
            ], 404);
        }

        // TODO Validação

        if (!$categoria->update($request->all())) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A categoria não pôde ser atualizada',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A categoria foi atualizada',
            'categoria' => $categoria,
            'show_categoria' => url()->route('categorias.show', ['categoria' => $categoria])
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Categoria $categoria
     */
    public function destroy($id)
    {
        $categoria = Categoria::find($id);

        if (!$categoria) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A categoria não pôde ser encontrada',
                'categoria' => null
            ], 404);
        }

        if (!$categoria->delete()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'A categoria não pôde ser removida',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'A categoria foi removida',
        ], 200);
    }
}
