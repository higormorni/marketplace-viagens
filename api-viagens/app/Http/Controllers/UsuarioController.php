<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::all();

        if ($usuarios->count() == 0) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhum usuário foi encontrado',
                'usuarios' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Os usuários foram listados com sucesso',
            'usuarios' => $usuarios
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO Validação
        $this->validate($request, [
            'email' => 'required|email|unique:usuarios,email,NULL,id',
        ]);

        $usuario = new Usuario();

        $usuario->fill($request->all());
        $usuario->senha = bcrypt($request->input('senha'));

        if (!$usuario->save()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O usuário não pôde ser criado',
            ], 500);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O usuário foi criado com sucesso',
            'usuario' => $usuario,
            'show_usuario' => url()->route('usuarios.show', ['usuario' => $usuario])
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Usuario $usuario
     */
    public function show($id)
    {
        $usuario = Usuario::find($id);

        if (!$usuario) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O usuário não foi encontrado',
                'usuario' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O usuário foi encontrado com sucesso',
            'usuario' => $usuario
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Usuario $usuario
     */
    public function update(Request $request, $id)
    {
        $usuario = Usuario::find($id);

        if (!$usuario) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O usuário não foi encontrado',
                'usuario' => null
            ], 404);
        }

        // TODO Validação


        if (!$usuario->update($request->all())) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O usuário não pôde ser atualizado',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O usuário foi atualizado',
            'usuario' => $usuario,
            'show_usuario' => url()->route('usuarios.show', ['usuario' => $usuario])
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Usuario $usuario
     */
    public function destroy($id)
    {
        $usuario = Usuario::find($id);

        if (!$usuario) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O usuário não pôde encontrado',
                'usuario' => null
            ], 404);
        }

        if (!$usuario->delete()) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'O usuário não pôde ser removido',
            ], 500);
        };

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'O usuário foi removido',
        ], 200);
    }
}
