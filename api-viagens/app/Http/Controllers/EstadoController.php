<?php

namespace App\Http\Controllers;

use App\Estado;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estado = Estado::all();

        if ($estado->count() == 0) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Nenhum estado encontrado',
                'estados' => []
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Estados listados com sucesso',
            'estados' => $estado
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param Estado $estado
     */
    public function show($id)
    {
        $estado = Estado::find($id);

        if(!$estado) {
            return response()->json([
                'status' => 'erro',
                'mensagem' => 'Estado não encontrado',
                'estado' => null
            ], 404);
        }

        return response()->json([
            'status' => 'sucesso',
            'mensagem' => 'Estado encontrado com sucesso',
            'estado' => $estado
        ], 200);
    }
}
