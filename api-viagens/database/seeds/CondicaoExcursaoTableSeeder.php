<?php

use Illuminate\Database\Seeder;
use App\CondicaoExcursao;

class CondicaoExcursaoTableSeeder extends Seeder {

	public function run()
	{
		DB::table('condicao_pagamento_excursao')->delete();

		// CondicaoPgtoExcursaoSeeder
		CondicaoExcursao::create([]);
	}
}