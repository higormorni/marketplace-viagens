<?php

use Illuminate\Database\Seeder;
use App\Categoria;

class CategoriasTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('categorias')->delete();

        DB::statement('ALTER TABLE categorias AUTO_INCREMENT = 1');

        $categorias = [
            ['nome' => 'Shows'],
            ['nome' => 'Teatros'],
            ['nome' => 'Festivais'],
            ['nome' => 'Passeios'],
            ['nome' => 'Feiras'],
            ['nome' => 'Conferências'],
        ];

        // CategoriasTableSeeder
        foreach ($categorias as $categoria) {
            Categoria::create($categoria);
        }
    }
}