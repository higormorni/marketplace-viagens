<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDestinosTable extends Migration {

	public function up()
	{
		Schema::create('destinos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('google_places_id')->unique();
			$table->integer('cidade_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('destinos');
	}
}