<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePacotesTable extends Migration {

	public function up()
	{
		Schema::create('pacotes', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('titulo');
			$table->text('descricao')->nullable();
			$table->decimal('preco')->nullable();
			$table->boolean('esgotado')->nullable()->default(false);
			$table->integer('excursao_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('pacotes');
	}
}