<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExcursoesTable extends Migration {

	public function up()
	{
		Schema::create('excursoes', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('titulo');
			$table->datetime('chegada')->nullable();
			$table->datetime('inicio')->nullable();
			$table->datetime('encerramento')->nullable();
			$table->datetime('previsao_retorno')->nullable();
			$table->enum('classificacao', array('L', '10', '12', '14', '16', '18'));
			$table->integer('categoria_id')->unsigned();
			$table->integer('destino_id')->unsigned();
			$table->integer('usuario_id')->unsigned()->nullable();
		});
	}

	public function down()
	{
		Schema::drop('excursoes');
	}
}