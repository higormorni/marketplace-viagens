<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCidadesTable extends Migration {

	public function up()
	{
		Schema::create('cidades', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
			$table->integer('estado_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('cidades');
	}
}