<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCondicoesTable extends Migration {

	public function up()
	{
		Schema::create('condicoes', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('condicao');
			$table->integer('usuario_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('condicoes');
	}
}