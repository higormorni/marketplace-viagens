<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCondicaoExcursaoTable extends Migration {

	public function up()
	{
		Schema::create('condicao_excursao', function(Blueprint $table) {
			$table->timestamps();
			$table->integer('condicao_id')->unsigned();
			$table->integer('excursao_id')->unsigned();

			$table->primary(['condicao_id', 'excursao_id']);
		});
	}

	public function down()
	{
		Schema::drop('condicao_excursao');
	}
}